package com.quarkus.course.resources;

import com.quarkus.course.dto.ProdutoDTO;
import com.quarkus.course.models.Produto;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Path("produtos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutoResource {

    @GET
    public List<Produto> buscarTodosProdutos() {
        return Produto.listAll();
    }

    @POST
    @Transactional
    public void cadastrarProduto(ProdutoDTO dto) {
        Produto produto = new Produto();
        produto.nome = dto.nome;
        produto.valor = dto.valor;
        produto.persist();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public void atualizarProduto(@PathParam("id") Long id, ProdutoDTO dto) {
        Optional<Produto> produtoOptional = Produto.findByIdOptional(id);

        if (produtoOptional.isPresent()) {
            Produto produto = produtoOptional.get();
            produto.nome = Objects.isNull(dto.nome) ? produto.nome : dto.nome;
            produto.valor = Objects.isNull(dto.valor) ? produto.valor : dto.valor;
            produto.persist();
        } else {
            throw new NotFoundException("Produto não encontrado");
        }
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public void deletarProduto(@PathParam("id") Long id) {
        Optional<Produto> produtoOptional = Produto.findByIdOptional(id);
        produtoOptional.ifPresentOrElse(Produto::delete, () -> {
            throw new NotFoundException("Produto não encontrado");
        });
    }
}
